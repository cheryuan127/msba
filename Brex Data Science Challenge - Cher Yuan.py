# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Brex Data Science Challenge
# ### Cher Yuan

# +
# import all the data files #
import numpy as np
import pandas as pd

customer_accounts = pd.read_csv('/customer_accounts.csv')
financials_accounts = pd.read_csv('/financials_accounts.csv')
financials_balances = pd.read_csv('/financials_balances.csv')
financials_transactions = pd.read_csv('/financials_transactions.csv')
# -

customer_accounts.info()

financials_accounts.info()

financials_balances.info()

# +
# rename some columns #
financials_balances.rename(columns={'amount':'balance_amount','accrual_date':'balance_date' }, inplace=True)
print(financials_balances.columns)

# change data type of date column #
financials_balances['balance_date'] = pd.to_datetime(financials_balances['balance_date'])
# -

financials_transactions.info()

# +
# rename some columns #
financials_transactions.rename(columns={'amount':'transaction_amount','accrual_date':'transaction_date' }, inplace=True)
print(financials_transactions.columns)

# change data type of date column #
financials_transactions['transaction_date'] = pd.to_datetime(financials_transactions['transaction_date'])

# +
# join customer_accounts.csv and financials_accounts.csv based on customer_account_id #
df_customer = pd.merge(customer_accounts,financials_accounts, left_on='id', right_on='customer_account_id', how="inner")

# drop duplicate columns and unnecessary columns #
df_customer.drop(df_customer.columns[[0, 1, 4]], axis=1, inplace=True)

df_customer.head()

# +
# In order to find out the number of bank accounts for each unique customer #
# group df_customer by customer_account_id and count the number of id of financial accounts #
customer_total_account = df_customer.groupby(['customer_account_id']).agg({'id_y':'count'}).reset_index()

# rename the column #
customer_total_account.rename(columns={'id_y':'total_number_of_bank_accounts' }, inplace=True)

customer_total_account.head()
# some customers do not have any financial account #

# +
# join df_customer and financials_balances based on account_id #
balance = pd.merge(df_customer,financials_balances, left_on='id_y', right_on='account_id', how="inner")


# drop duplicate columns and uncessary columns #
balance.drop(balance.columns[[0, 2]], axis=1, inplace=True)


balance.head()

# +
# group by balance_amount data based on customer_account_id and balance_date #
balance_sum = balance.groupby(['customer_account_id','balance_date']).agg({'balance_amount':'sum'}).reset_index()

balance_sum.head()


# +
# join df_customer and financials_transcations based on account_id #
df_transaction = pd.merge(df_customer,financials_transactions, left_on='id_y', right_on='account_id', how="inner")

# drop duplicate columns and unnecessary columns #
df_transaction.drop(df_transaction.columns[[0, 2]], axis=1, inplace=True)

df_transaction.head()


# +
# group by transaction_amount data based on customer_account_id and transaction_date #
transaction_sum = df_transaction.groupby(['customer_account_id','transaction_date']).agg({'transaction_amount':'sum'}).reset_index()

transaction_sum.head()
# -

# sort aggreated balance data by balance_date #
balance_sum.sort_values(by='balance_date')

# sort aggregated transaction data by transaction_date #
transaction_sum.sort_values(by='transaction_date')

# find the earliest balance_date for each customer account #
balance_date = balance_sum.sort_values(by=['balance_date']).drop_duplicates(subset='customer_account_id', keep='first')
balance_date

# find the earliest transaction_date for each customer account #
transaction_date = transaction_sum.sort_values(by=['transaction_date']).drop_duplicates(subset='customer_account_id', keep='first')
transaction_date

# +
# As we can see, the time range for aggregated balance data of all the customer accounts is from 2017/10/13 to 2018/09/22
# On the other hand, the time range for aggregated transaction data is from 2017/09/28 to 2018/09/21
# Therefore, it is reasonable to use 2017/09/28 as the start date for all the accounts.
# The end date for all the accounts in the datasets should be 2018/09/22.
# If there are missing calendar days between the associated time range for each account, 
# either the related balance_amount or the related transcation_amount for that day would be interpolated with $0.

# +
# join balance_date and ranscation_date based on customer_account_id
data1 = pd.merge(balance_date,transaction_date, left_on='customer_account_id', right_on='customer_account_id', how="outer")

#check data
data1

# +
# create complete dates between 2017-09-28 and 2018-09-22 #
import datetime
start = datetime.datetime(2017, 9, 28)
end = datetime.datetime(2018, 9, 22)

dates = pd.date_range(start, end)

dates


# +
# For each customer_account_id, fill in days between 2017-09-28 and 2018-09-22 #
df = data1.groupby('customer_account_id').apply(lambda x:pd.DataFrame(dates,columns=['date']))
df = df.reset_index(level=0)

df.head()
# -

data1.shape[0]*len(dates)==df.shape[0]

# +
# For each customer_account_id, if there is aggregated balance data for the given day, bring in the data from balance_sum data frame.#
# Otherwise, the missing value for the given day would be $0.#

df1 = pd.merge(df,balance_sum, left_on=['customer_account_id','date'],
               right_on=['customer_account_id','balance_date'],how='left')
df1.drop(['balance_date'], axis=1, inplace=True)
df1 = df1.fillna(0)

df1.head()

# +
# For each customer_account_id, if there is aggregated transaction data for the given day, bring in the data from transaction_sum data frame.#
# Otherwise, the missing value for the given day would be $0.#

df2 = pd.merge(df1,transaction_sum, left_on=['customer_account_id','date'],
               right_on=['customer_account_id','transaction_date'],how='left')
df2.drop(['transaction_date'], axis=1, inplace=True)
df2 = df2.fillna(0)

df2.head()

# +
# The total daily balance for each customer_account_id is the sum of balance_amount and transaction_amount.#
df2['total_balance'] = (df2['balance_amount'] + df2['transaction_amount'])


df2.head()

# +
# drop unnecessary columns #
df2.drop(df2.columns[[2, 3]], axis=1, inplace=True)


df2.info()

# +
#convert cents to dollars#
df2['total_balance'] = df2['total_balance']/100


# rename the column #
df2.rename(columns={'total_balance':'total_balance_in_dollars' }, inplace=True)

# add dollar sign#
pd.options.display.float_format = '${:,.2f}'.format

df2

# -

df2.to_csv('/result.csv',encoding='utf-8-sig',index=False)


