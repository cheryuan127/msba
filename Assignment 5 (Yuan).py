# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.4
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Assignment 5
# ### Cher Yuan

# #### Q1: Using pandas, import the hotels.csv Preview the documentfile and show the number of observations and columns.

# +
import pandas as pd

hotels = pd.read_csv('C:/Users/chery/Desktop/ProgramingforAnalytics/assignment_data/hotels.csv')
hotels.head(10) # preview the documentfile#
# -

hotels.shape # get the number of observations and columns #

# There are 366,644 observations and 31 columns in the dataset.

# #### Q2: Write a Python program to determine the number of hotel types and how many of each type.

print(hotels['hotel_type'].value_counts().count())

# There are 3 hotel types.

print(hotels['hotel_type'].value_counts()) # get the number of observations for each hotel type #

# There are 184,421 hotels, 119,674 B&B, and 62,549 others.

# #### Q3: Using a dataframe.method1().method2() syntax, count the number of missing values in each column.

hotels.isna().sum() 

# There are 31421 missing values for "rooms", 22518 for "bubble_rating", 42 for "page_position", 42 for "out_of", 31421 for "reviews_per_room", 22363 for "management_response_rate", and 31423 for "traffic_per_room". 

# #### Q4.  For this assignment let's define a continuous variable as any numeric (integer and float) variable that has more than 10 distinct values in its distribution. Produce a table showing the mean, standard deviation, skewness and kurtosis of all the continuous variables.

# +
# first, we want to select all the numeric variables #
numeric_variables = hotels.select_dtypes(include = ['int64', 'float64'])


# For those numeric variables, we are only interested in those with more than 10 distinct values #
for variables in numeric_variables:
    if hotels[variables].nunique() > 10:
        print(variables, ": ")
        print(hotels[variables].agg(['mean', 'std', 'skew', 'kurt']))
# -

# #### Q5: Find and replace all continuous variable outliers with missing values.

# +
import numpy as np

for variables in numeric_variables:
    if hotels[variables].nunique() > 10:
        hotels[variables] = np.where(np.abs(hotels[variables] - hotels[variables].mean()) > (3 * hotels[variables].std()),
                              np.nan, hotels[variables])

        
# check the dataset after replacing outliers with missing values #
print(hotels.head(25))
# -

# #### Q6: Impute all continuous variable missing values with the means of those variables.

# +
# mean inputation
for variables in numeric_variables:
     if hotels[variables].nunique() > 10:
            hotels[variables].fillna(value = hotels[variables].mean(), inplace = True)

            
# check the dataset after mean imputation #
print(hotels.head(25))
# -

# #### Q7: How has the kurtosis changed for each of the variables that had outliers removed and then imputed?

# compute kurtosis after mean imputation #
for variables in numeric_variables:
    if hotels[variables].nunique() > 10:
        print(variables, ": ")
        print(hotels[variables].agg(['kurt']))


# The kurtosis has significantly decreased for each of the variables that had outliers removed and then imputed.
